package com.example.snack_mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.Carousel;
import com.example.snack_mall.entity.ForYou;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface IForYouService extends IService<ForYou> {
    //添加
    Result reg(ForYou forYou);

    //编辑
    Result edit(ForYou forYou);

    //删除
    Result delete(int id);

    //批量删除
    Result deleteMany(List<Integer> ids);


    Result findPage(Page<ForYou> page);

    Result forYouGoods();
}
