package com.example.snack_mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.dto.OrderDto;
import com.example.snack_mall.entity.OrderItem;
import com.example.snack_mall.entity.OrderMsg;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.snack_mall.entity.ShoppingItem;
import com.example.snack_mall.vo.OrderMsgVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface IOrderService extends IService<OrderMsg> {

    Result reg(OrderDto orderDto);

    Result del(List<Integer> ids);

    Result edit(OrderMsg orderMsg);

    Result dis(List<Integer> ids);

    Result out(List<Integer> id);

    Result findPage(Page<OrderMsg> page);

    Result orderPeople(int id);
//我的订单分页
    Result Page(Page<OrderMsgVo> page, Integer userId);

    Result address(Integer userId);

    Result pay(String orderNo);

    Result delete(int id);

    Result isOver(int id);
}
