package com.example.snack_mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.New;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface INewService extends IService<New> {
    //添加
    Result reg(New fnew);

    //编辑
    Result edit(New fnew);

    //删除
    Result delete(int id);

    //批量删除
    Result deleteMany(List<Integer> ids);

    //分页查询
    Result findPage(Page<New> page);

    Result newGoods();
}
