package com.example.snack_mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.GoodsInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface IGoodsInfoService extends IService<GoodsInfo> {
    //添加
    Result reg(GoodsInfo goodsInfo);

    //编辑
    Result edit(GoodsInfo goodsInfo);

    Result updateGoodsInfo(GoodsInfo goodsInfo);

    Result goodsByCategoryId(int id);


    //商品管理分页
    Result findPage(Page<GoodsInfo> page);

    //根据商品分类编号查询商品信息
    Result goodsMsg(int id);



    Result selectShop();
    //上架状态
    Result editGoodsSellStatus(GoodsInfo goodsInfo);

    Result selectShopMsgById(int id);

    Result selectGoodsBySearch(String msg);

    List<GoodsInfo> goodsInfo(ArrayList<Integer> goodsId);

    boolean clear(ArrayList<Integer> ids);
}
