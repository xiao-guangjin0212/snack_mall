package com.example.snack_mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.Hot;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface IHotService extends IService<Hot> {


    /**
     * 新增热销商品
     * @param hot:热销商品实体类
     * @return :返回新增结果
     */
    Result insert(Hot hot);


    /**
     * 修改热销商品
     * @param hot :热销商品实体类
     * @return :返回修改结果
     */
    Result updateHot(Hot hot);

    Result deleteHot(int id);

    Result findPage(Page<Hot> page);

    Result deleteMany(List<Integer> ids);

    Result hotGoods();

//    Result findPage(Page<Hot> page);
}
