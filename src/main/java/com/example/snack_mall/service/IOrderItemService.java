package com.example.snack_mall.service;

import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.OrderItem;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Service
public interface IOrderItemService extends IService<OrderItem> {

        Result orderMsg(int id);
}
