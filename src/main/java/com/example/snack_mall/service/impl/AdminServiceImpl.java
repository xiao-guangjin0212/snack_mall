package com.example.snack_mall.service.impl;

import cn.hutool.core.lang.UUID;
import cn.hutool.crypto.digest.DigestUtil;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.dto.AdminDto;
import com.example.snack_mall.entity.Admin;
import com.example.snack_mall.mapper.AdminMapper;
import com.example.snack_mall.service.IAdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snack_mall.util.AdminHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {

    @Override
    public Result login(Admin admin, HttpSession session) {
        //密码转换为md5加密之后的值
        String MD5Password = DigestUtil.md5Hex(admin.getAdminPassword());
        //查询数据库
        Admin user = query().eq("admin_account",admin.getAdminAccount())
                    .eq("admin_password",MD5Password).one();
        //没有查询到，密码错误
        if (user == null){
            return Result.fail("用户名密码错误！");
        }
        //获得token
        String token = UUID.randomUUID().toString(true);
        //登录人员信息
        session.setAttribute(token,user);
        AdminHolder.setUser(user);
        //返回token
        return Result.ok("登陆成功！",token);
    }

    @Override
    public Result updateMsg(Admin admin) {
        boolean b = this.updateById(admin);
        if (!b){
            return Result.fail("修改登录信息失败！");
        }
        Admin res = this.query().eq("admin_id", AdminHolder.getUser().getAdminId()).one();
        AdminHolder.setUser(res);
        return Result.ok("修改登录信息成功！");
    }

    @Override
    public Result updatePassword(AdminDto adminDto) {
        String s = DigestUtil.md5Hex(adminDto.getOldAdminPassword());
        if (!AdminHolder.getUser().getAdminPassword().equals(s)){
            return Result.fail("原密码不正确！");
        }
        Admin a = new Admin();
        a.setAdminId(AdminHolder.getUser().getAdminId());
        a.setAdminPassword(DigestUtil.md5Hex(adminDto.getNewAdminPassword()));
        boolean b = updateById(a);
        if (!b){
            return Result.fail("密码修改失败，请稍后重试！");
        }
        AdminHolder.setUser(this.query().eq("admin_id",AdminHolder.getUser().getAdminId()).one());
        return Result.ok("密码修改成功！");
    }

    @Override
    public Result info(String token, HttpSession session) {
        return  Result.ok("获取登录信息成功",session.getAttribute(token));
    }

}
