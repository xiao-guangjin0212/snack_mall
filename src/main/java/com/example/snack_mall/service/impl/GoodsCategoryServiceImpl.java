package com.example.snack_mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.GoodsCategory;
import com.example.snack_mall.mapper.GoodsCategoryMapper;
import com.example.snack_mall.service.IGoodsCategoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snack_mall.util.AdminHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Service
public class GoodsCategoryServiceImpl extends ServiceImpl<GoodsCategoryMapper, GoodsCategory> implements IGoodsCategoryService {

    @Override
    public Result goodsByParent(int id) {
        QueryWrapper<GoodsCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id",id);
        List<GoodsCategory> goodsCategories = this.getBaseMapper().selectList(queryWrapper);
        return Result.ok(goodsCategories);
    }
    @Override
    public Result insert(GoodsCategory goodsCategory) {
        goodsCategory.setAddPeople(AdminHolder.getUser().getAdminId());
        boolean save = this.save(goodsCategory);
        if(save){
            return Result.ok("添加成功");
        }else {
            return Result.fail("添加失败");
        }
    }

    @Override
    public Result updateCate(GoodsCategory goodsCategory) {
        boolean updateCate = this.updateById(goodsCategory);
        if (updateCate){
            return Result.ok("修改成功");
        }else {
            return Result.fail("修改失败");
        }
    }

    @Override
    public Result deleteCategory(int id) {
        List<GoodsCategory> parent = this.query().eq("parent_id", id).list();
        if (parent.size()!=0){
            return Result.fail("666");
        }
        boolean deleteCategory = this.removeById(id);
        if (deleteCategory){
            return Result.ok("删除成功");
        }else {
            return Result.fail("删除失败");
        }
    }

    @Override
    public Result goodsCategory() {
        List<GoodsCategory> all = this.query().eq("is_deleted", 0).list();
        List<GoodsCategory> first = all.stream().filter(g->g.getCategoryLevel()==1).collect(Collectors.toList());

        for (GoodsCategory one : first){
            List<GoodsCategory> second = new ArrayList<>();
            for (GoodsCategory g : all){
                if (g.getCategoryLevel().equals(2) && g.getParentId().equals(one.getCategoryId())){
                    second.add(g);
                }
            }
            one.setChildren(second);

            for (GoodsCategory two : second){
                    List<GoodsCategory> third = new ArrayList<>();
                    for (GoodsCategory g : all){
                        if (g.getCategoryLevel().equals(3) && g.getParentId().equals(two.getCategoryId())){
                            third.add(g);
                        }
                    }
                    two.setChildren(third);
            }
        }


        return Result.ok(first);
    }

    @Override
    public Result findPage(Page<GoodsCategory> page) {
        Page<GoodsCategory> p = this.query().eq("is_deleted",0).page(page);
        return Result.ok(p);
    }

    @Override
    public Result deleteMany(List<Integer> ids) {
        List<GoodsCategory> parent = this.query().in("parent_id", ids).list();
        if (parent.size()!=0){
            return Result.fail("666");
        }
        boolean b = this.update().set("is_deleted",1).in("category_id",ids).update();
        if (b){
            return Result.ok("删除成功");
        }
        return Result.fail("删除失败");
    }

    @Override
    public Result selectLevel() {
        List<GoodsCategory> res = this.query().in("category_level", 1,2).list();
        return Result.ok(res);
    }

    @Override
    public Result selectFather(int id) {
        GoodsCategory res = this.query().eq("category_id", id).one();
        return Result.ok(res);
    }

}
