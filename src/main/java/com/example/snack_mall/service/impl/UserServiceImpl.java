package com.example.snack_mall.service.impl;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.PhoneUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.dto.UserDto;
import com.example.snack_mall.entity.Admin;
import com.example.snack_mall.entity.User;
import com.example.snack_mall.mapper.UserMapper;
import com.example.snack_mall.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snack_mall.util.UserHolder;
import com.example.snack_mall.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import springfox.documentation.spring.web.json.Json;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Result reg(User user) {
        boolean save = this.save(user);
        if (!save){
            return Result.fail("注册失败，请稍后重试！");
        }
        return Result.ok("注册成功！请登录！");
    }

    @Override
    public Result lock(List <Integer> ids,Integer lockedFlag) {
        UpdateWrapper<User> UpdateWrapper= new UpdateWrapper<>();
        UpdateWrapper.in("user_id",ids)
                .set("locked_flag",lockedFlag)
                .set("addtime", LocalDateTime.now());
        boolean lock = this.update(UpdateWrapper);
        if (!lock){
            if (lockedFlag == 0){
                return Result.fail("禁用失败！");
            }else {
                return Result.fail("解除失败！");
            }
        }else {
            if (lockedFlag == 0){
                return  Result.ok("禁用成功");
            }else {
                return  Result.ok("解除成功");
            }
        }
    }


    public Result cancel(List <Integer> ids,Integer isDeleted) {
        UpdateWrapper<User> UpdateWrapper= new UpdateWrapper<>();
        UpdateWrapper.in("user_id",ids)
                .set("is_deleted",isDeleted)
                .set("addtime", LocalDateTime.now());
        boolean cancel = update(UpdateWrapper);
        if (!cancel){
            if (isDeleted == 0){
                return Result.fail("禁用失败！");
            }else {
                return Result.fail("解除失败！");
            }
        }else {
            if (isDeleted == 0){
                return  Result.ok("禁用成功");
            }else {
                return  Result.ok("解除成功");
            }
        }
    }


    @Override
    public Result edit(User user) {
        boolean b = this.updateById(user);
        if (!b){
            return Result.fail("编辑信息失败，请稍后重试！");
        }
        return Result.ok("编辑信息成功！");
    }

    @Override
    public Result findPage(Page<User> p) {
        Page<User> page = this.page(p);
        return Result.ok(page);
    }

    @Override
    public Result login(User user, HttpSession session) {
        UserVo vo = new UserVo();
        //密码转换为md5加密之后的值
        String MD5Password = DigestUtil.md5Hex(user.getPasswordMd5());
        //查询数据库
        User res = this.query().eq("login_name",user.getLoginName())
                .eq("password_md5",MD5Password).one();
        //没有查询到，密码错误
        if (res == null){
            return Result.fail(" Wrong account number or password！");
        }
        //获得token
        String token = UUID.randomUUID().toString(true);
        vo.setToken(token);
        vo.setUser(res);
        //登录人员信息
        stringRedisTemplate.opsForValue().set("login:user:"+token, JSONUtil.toJsonStr(user),30L,TimeUnit.MINUTES);
        UserHolder.setUser(res);
        return Result.ok(vo);
    }

    @Override
    public Result send(User user) {
        if (StrUtil.isBlank(user.getLoginName())){
            return Result.fail("手机号码不能为空");
        }
        if (!PhoneUtil.isMobile(user.getLoginName())){
            return Result.fail("手机号码格式有误！");
        }
        //获取六位验证码
        String s = RandomUtil.randomNumbers(6);
        //发送验证码
        System.out.println("phone:" + s);
        stringRedisTemplate.opsForValue().set("login:code:"+user.getLoginName(),s,2L, TimeUnit.MINUTES);

        return Result.ok(s);
    }

    @Override
    public Result register(UserDto userDto) {
        String s = stringRedisTemplate.opsForValue().get("login:code:"+userDto.getUser().getLoginName());
        if (StrUtil.isBlank(s)){
            return Result.fail("手机号或验证码有误！");
        }
        if (Integer.parseInt(s) != userDto.getCode()){
            return Result.fail("验证码不正确！");
        }
        stringRedisTemplate.delete("login:code:" + userDto.getUser().getLoginName());
        String s1 = DigestUtil.md5Hex(userDto.getUser().getPasswordMd5());
        userDto.getUser().setPasswordMd5(s1);
        boolean b = this.save(userDto.getUser());
        if (b){
            return Result.ok("注册成功！");
        }else {
            return Result.fail("注册失败！");
        }
    }
}
