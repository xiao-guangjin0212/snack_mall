package com.example.snack_mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.GoodsInfo;
import com.example.snack_mall.entity.ShoppingItem;
import com.example.snack_mall.mapper.GoodsInfoMapper;
import com.example.snack_mall.mapper.ShoppingItemMapper;
import com.example.snack_mall.service.IShoppingItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Service
public class ShoppingItemServiceImpl extends ServiceImpl<ShoppingItemMapper, ShoppingItem> implements IShoppingItemService {

    @Resource
    ShoppingItemMapper shoppingItemMapper;

    @Resource
    GoodsInfoServiceImpl goodsInfoService;

    @Override
    public List<ShoppingItem> selectCart(int id) {
        List<ShoppingItem> shoppingItem = shoppingItemMapper.selectCart(id);
        return shoppingItem;
    }

    @Override
    public Result addCart(ShoppingItem shoppingItem) {
        boolean save = this.save(shoppingItem);
        if (!save){
            return Result.fail("添加购物车失败！");
        }
        return Result.ok("添加购物车成功！");
    }

    @Override
    public Result add(int cartItemId,int goodsCount) {
        ShoppingItem cart = this.query().eq("cart_item_id", cartItemId).one();
        GoodsInfo goods = goodsInfoService.query().eq("goods_id", cart.getGoodsId()).one();
        if (goodsCount>=goods.getStockNum()){
            return Result.ok("商品库存不足！");
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("cart_item_id",cartItemId);
        ShoppingItem a = new ShoppingItem();
        a.setGoodsCount(goodsCount+1);
        boolean update = this.update(a, queryWrapper);
        if (update){
            return Result.ok("增加成功！");
        }
        return Result.fail("增加失败！");
    }

    @Override
    public Result minus(int cartItemId, int goodsCount) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("cart_item_id",cartItemId);
        ShoppingItem a = new ShoppingItem();
        a.setGoodsCount(goodsCount-1);
        boolean update = this.update(a, queryWrapper);
        if (update){
            return Result.ok("减少成功！");
        }
        return Result.fail("减少失败！");
    }

    @Override
    public Result delete(int id) {
        QueryWrapper<ShoppingItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cart_item_id",id);
        boolean remove = this.remove(queryWrapper);
        if (remove){
            return Result.ok("删除成功");
        }
        return Result.fail("删除失败");
    }

    @Override
    public Result deleteMany(List<Integer> ids) {
        QueryWrapper<ShoppingItem> queryWrapper =new QueryWrapper<>();
        queryWrapper.in("cart_item_id",ids);
        boolean remove = this.remove(queryWrapper);
        if (remove){
            return Result.ok("删除成功");
        }
        return Result.fail("删除失败");
    }

    @Override
    public Result clear(Integer userId) {
        List<ShoppingItem> carts = this.query().eq("user_id", userId).list();
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < carts.size(); i++) {
             list.add(carts.get(i).getGoodsId());
        }
        List<GoodsInfo> goodsInfos = goodsInfoService.goodsInfo(list);
        ArrayList<Integer> ids = new ArrayList<>();
        for (int i = 0; i < goodsInfos.size(); i++) {
            if (goodsInfos.get(i).getGoodsSellStatus() == 0){
                ids.add(goodsInfos.get(i).getGoodsId());
            }
        }
        if (ids.size() == 0){
            return Result.ok("成功清除下架商品");
        }
        boolean clear = goodsInfoService.clear(ids);
        if (!clear){
            return Result.fail("清除下架商品失败");
        }
        return Result.ok("成功清除下架商品");
    }
}
