package com.example.snack_mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.GoodsInfo;
import com.example.snack_mall.mapper.GoodsInfoMapper;
import com.example.snack_mall.service.IGoodsInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Service
public class GoodsInfoServiceImpl extends ServiceImpl<GoodsInfoMapper, GoodsInfo> implements IGoodsInfoService {
    //添加
    @Override
    public Result reg(GoodsInfo goodsInfo) {
        boolean save = this.save(goodsInfo);
        if (!save) {
            return Result.fail("添加失败，请稍后重试！");
        }
        return Result.ok("添加成功");
    }
    //编辑
    @Override
    public Result edit(GoodsInfo goodsInfo) {
        boolean b = this.updateById(goodsInfo);
        if (!b) {
            return Result.fail("编辑商品管理失败，请稍后重试");
        }
        return Result.ok("编辑商品管理");
    }

    @Override
    public Result updateGoodsInfo(GoodsInfo goodsInfo) {
        boolean b = this.updateById(goodsInfo);
        if (!b){
            return Result.fail("商品信息编辑失败，请稍后重试！");
        }
        return Result.ok("商品信息编辑成功！");
    }

    @Override
    public Result goodsByCategoryId(int id) {
        List<GoodsInfo> res = this.query().eq("goods_category_id", id).list();
        if (res==null){
            return Result.fail("未查询到商品！");
        }
        return Result.ok(res);
    }


    //分页查询
    @Override
    public Result findPage(Page<GoodsInfo> page) {
        Page<GoodsInfo> page1 = this.query().page(page);
        return Result.ok(page1);
    }

    @Override
    public Result goodsMsg(int id) {
        GoodsInfo msg = this.query().eq("goods_category_id", id).one();
        if (msg!=null) {
            return Result.ok(msg);
        }else {
            return Result.fail("未找到商品");
        }
    }


    @Override
    public Result selectShop() {
        List<GoodsInfo> shop = this.query().list();
        return Result.ok(shop);
    }
    //上架
    @Override
    public Result editGoodsSellStatus(GoodsInfo goodsInfo) {
        boolean b = this.updateById(goodsInfo);
        if (!b){
                return Result.fail("变更上下架状态失败");
        }else {
                return Result.ok("ok");
        }

    }

    @Override
    public Result selectShopMsgById(int id) {
        GoodsInfo msg = this.query().eq("goods_id", id).one();
        if (msg!=null) {
            return Result.ok(msg);
        }else {
            return Result.fail("未找到商品");
        }
    }

    @Override
    public Result selectGoodsBySearch(String msg) {
        List<GoodsInfo> list = this.query().like("goods_name", msg).or().like("goods_intro", msg).or().like("selling_price", msg).list();
        return Result.ok(list);
    }

    @Override
    public List<GoodsInfo> goodsInfo(ArrayList<Integer> goodsId) {
        List<GoodsInfo> goods = this.query().in("goods_id", goodsId).list();
        return goods;
    }

    @Override
    public boolean clear(ArrayList<Integer> ids) {
        QueryWrapper<GoodsInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("goods_id",ids);
        boolean remove = this.remove(queryWrapper);
        return remove;
    }

}
