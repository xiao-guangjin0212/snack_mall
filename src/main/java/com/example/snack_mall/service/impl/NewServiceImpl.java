package com.example.snack_mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.GoodsInfo;
import com.example.snack_mall.entity.New;
import com.example.snack_mall.mapper.NewMapper;
import com.example.snack_mall.service.INewService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snack_mall.util.AdminHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Service
public class NewServiceImpl extends ServiceImpl<NewMapper, New> implements INewService {
    @Resource
    private NewMapper newMapper;
    //添加
    @Override
    public Result reg(New fnew) {
        fnew.setAddPeople(AdminHolder.getUser().getAdminId());
        boolean save = this.save(fnew);
        if (!save) {
            return Result.fail("添加失败，请稍后重试！");
        }
        return Result.ok("添加成功");
    }

    //编辑
    @Override
    public Result edit(New fnew) {
        boolean b = this.updateById(fnew);
        if (!b) {
            return Result.fail("编辑新品上线失败，请稍后重试");
        }
        return Result.ok("编辑新品成功");
    }

    //删除
    @Override
    public Result delete(int id) {
        QueryWrapper<New> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("new_id", id);
        New a = new New();
        a.setIsDelete(1);
        boolean update = this.update(a, queryWrapper);
        if (!update) {
            return Result.fail("删除新品上线失败，请稍后重试");
        }
        return Result.ok("删除新品成功");
    }

    //批量删除
    @Override
    public Result deleteMany(List<Integer> ids) {
        QueryWrapper<New> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("new_id", ids);
        New a = new New();
        a.setIsDelete(1);
        boolean update = this.update(a,queryWrapper);
        if (!update) {
            return Result.fail("批量删除新品上线失败，请稍后重试");
        }
        return Result.ok("批量删除新品成功");
    }

    //分页查询
    @Override
    public Result findPage(Page<New> page) {
        Page<New> page1 = this.query().eq("is_delete",0).page(page);
        return Result.ok(page1);
    }

    @Override
    public Result newGoods() {
        List<GoodsInfo> goodsInfos = newMapper.newGoods();
        if (goodsInfos==null){
            return Result.fail("新品推荐加载失败！");
        }
        return Result.ok(goodsInfos);
    }


}
