package com.example.snack_mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.Carousel;
import com.example.snack_mall.mapper.CarouselMapper;
import com.example.snack_mall.service.ICarouselService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snack_mall.util.AdminHolder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-02
 */
@Service
public class CarouselServiceImpl extends ServiceImpl<CarouselMapper, Carousel> implements ICarouselService {

    @Override
    public Result reg(Carousel carousel) {
        carousel.setAddPeople(AdminHolder.getUser().getAdminId());
        boolean save = this.save(carousel);
        if (!save){
            return Result.fail("新增轮播图失败，请稍后重试！");
        }
        return Result.ok("新增轮播图成功！");
    }

    @Override
    public Result del(int id) {
        QueryWrapper<Carousel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("carousel_id",id);
        Carousel a =new Carousel();
        a.setIsDeleted(1);
        boolean b = this.update(a,queryWrapper);
        if (!b){
            return Result.fail("删除轮播图失败，请稍后重试！");
        }
        return Result.ok("删除轮播图成功！");
    }

    @Override
    public Result deleteMany(List<Integer> ids) {
        QueryWrapper<Carousel> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("carousel_id",ids);
        Carousel a =new Carousel();
        a.setIsDeleted(1);
        boolean b = this.update(a,queryWrapper);
        if (!b){
            return Result.fail("批量删除轮播图失败，请稍后重试！");
        }
        return Result.ok("批量删除轮播图成功！");
    }

    @Override
    public Result edit(Carousel carousel) {
        boolean b = this.updateById(carousel);
        if (!b){
            return Result.fail("编辑轮播图失败，请稍后重试！");
        }
        return Result.ok("编辑轮播图成功！");
    }

    @Override
    public Result findPage(Page<Carousel> page1) {
        Page<Carousel> res = this.query().eq("is_deleted",0).orderByDesc("addtime").page(page1);
        return Result.ok(res);
    }

    @Override
    public Result carouselUrl() {
        List<Carousel> carousel = this.query().orderByDesc("carousel_rank").list();
        if (carousel == null){
            return Result.fail("轮播图加载失败！");
        }
        return Result.ok(carousel);
    }
}
