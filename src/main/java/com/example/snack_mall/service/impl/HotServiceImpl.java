package com.example.snack_mall.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.GoodsInfo;
import com.example.snack_mall.entity.Hot;
import com.example.snack_mall.mapper.HotMapper;
import com.example.snack_mall.service.IHotService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snack_mall.util.AdminHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Service
public class HotServiceImpl extends ServiceImpl<HotMapper, Hot> implements IHotService {
    @Resource
    private HotMapper hotMapper;

    @Override
//    public Result insert(Hot hot) {
//        hot.setAddPeople(AdminHolder.getUser().getAdminId());
//        boolean save = this.save(hot);
//        if (save){
//            return Result.ok("新增热销商品成功");
//        }else {
//            return Result.fail("新增热销商品失败");
//        }
//    }
    public Result insert(Hot hot) {
        hot.setAddPeople(AdminHolder.getUser().getAdminId());
        boolean save = this.save(hot);
        if (save){
            return Result.ok("新增热销商品成功");
        }else {
            return Result.fail("新增热门商品失败");
        }
    }

    @Override
    public Result updateHot(Hot hot) {
        boolean update = this.updateById(hot);
        if (update){
            return Result.ok("修改热销商品成功");
        }else {
            return Result.fail("修改热销商品失败");
        }
    }

    @Override
    public Result deleteHot(int id) {
        boolean deleteHot = this.removeById(id);
        if (deleteHot){
            return Result.ok("删除热销商品成功");
        }else {
            return Result.fail("删除热销商品失败");
        }
    }

    @Override
    public Result findPage(Page<Hot> page) {
        Page<Hot> p = this.query().eq("is_deleted",0).page(page);
        return Result.ok(p);
    }

    @Override
    public Result deleteMany(List<Integer> ids) {
        boolean b = this.update().set("is_deleted", 1).in("hot_id", ids).update();
        if (b){
            return Result.ok("删除成功！");
        }
        return Result.fail("删除失败！");
    }

    @Override
    public Result hotGoods() {
        List<GoodsInfo> goodsInfos = hotMapper.hotGoods();
        if (goodsInfos==null){
            return Result.fail("加载热销商品失败！");
        }
        return Result.ok(goodsInfos);
    }


}
