package com.example.snack_mall.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.dto.OrderDto;
import com.example.snack_mall.entity.*;
import com.example.snack_mall.mapper.OrderItemMapper;
import com.example.snack_mall.mapper.OrderMapper;
import com.example.snack_mall.service.IOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snack_mall.vo.OrderMsgVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, OrderMsg> implements IOrderService {

    @Resource
    private OrderMapper orderMapper;
    @Resource
    private OrderItemMapper orderItemMapper;
    @Resource
    private OrderItemServiceImpl orderItemService;
    @Resource
    private GoodsInfoServiceImpl goodsInfoService;

    @Override
    public Result reg(OrderDto orderDto) {
        String s = RandomUtil.randomNumbers(17);
        orderDto.getOrderMsg().setOrderNo(s);
        boolean save = this.save(orderDto.getOrderMsg());
        OrderMsg order = this.query().eq("order_no", s).one();
        ArrayList<OrderItem> a = new ArrayList<>();
        OrderItem b = new OrderItem();
        for (int i = 0; i < orderDto.getShoppingItems().size(); i++) {
            b.setGoodsCount(orderDto.getShoppingItems().get(i).getGoodsCount());
            b.setGoodsId(orderDto.getShoppingItems().get(i).getGoodsInfo().getGoodsId());
            b.setOrderId(order.getOrderId());
            b.setGoodsName(orderDto.getShoppingItems().get(i).getGoodsInfo().getGoodsName());
            b.setGoodsCoverImg(orderDto.getShoppingItems().get(i).getGoodsInfo().getGoodsCoverImg());
            b.setSellingPrice(orderDto.getShoppingItems().get(i).getGoodsInfo().getSellingPrice());
            a.add(b);
        }
        orderItemService.saveBatch(a);

        if (!save){
            return Result.fail("订单新增失败！");
        }
        return Result.ok(s);
    }

    @Override
    public Result del(List<Integer> ids) {
        boolean del = this.update().set("is_deleted", 1).in("order_id",ids).update();
        if (!del){
            return Result.fail("订单关闭失败，请稍后重试！");
        }
        return Result.ok("订单关闭成功！");
    }

    @Override
    public Result edit(OrderMsg orderMsg) {
        boolean edit = this.updateById(orderMsg);
        if (!edit){
            return Result.fail("修改失败，请稍后重试！");
        }
        return Result.ok("修改成功！");
    }

    @Override
    public Result dis(List<Integer> ids) {
        List<OrderMsg> order = this.query().in("order_id", ids).list();
        for(OrderMsg o : order){
            if (!o.getPayStatus().equals("已支付")){
                return Result.fail("有订单不是支付成功状态，无法配货！");
            }
        }
        boolean b = this.update().set("pay_status", "配货完成").in("order_id", ids).update();
        if (!b){
            return Result.fail("配货失败，请稍后重试！");
        }
        return Result.ok("配货成功！");
    }

    @Override
    public Result out(List<Integer> ids) {
        List<OrderMsg> order = this.query().in("order_id", ids).list();
        for (OrderMsg o : order){
            if (!o.getPayStatus().equals("配货完成")){
                return Result.fail("有订单不是配货完成状态，无法出库！");
            }
        }
        boolean b = this.update().set("pay_status", "已出库").in("order_id", ids).update();
        if (!b){
            return Result.fail("出库失败，请稍后重试！");
        }
        return Result.ok("出库成功！");
    }

    @Override
    public Result findPage(Page<OrderMsg> page) {
        Page<OrderMsg> res = this.query().eq("is_deleted",0).orderByDesc("addtime").page(page);
        return Result.ok(res);
    }

    @Override
    public Result orderPeople(int id) {
        OrderMsg res = this.query().eq("order_id",id).one();
        return Result.ok(res);
    }

    @Override
    public Result Page(Page<OrderMsgVo> page,Integer userId) {
        PageHelper.startPage((int)page.getCurrent(),(int)page.getSize());
        List<OrderMsgVo> orderMsgs = orderItemMapper.selectOrderByUserId(userId);
        if (orderMsgs==null){
            return Result.fail("订单信息加载失败！");
        }

        PageInfo<OrderMsgVo> pi = new PageInfo<>(orderMsgs);
        return Result.ok(pi);
    }

    @Override
    public Result address(Integer userId) {
        List<OrderMsg> userAddress = this.query().eq("user_id", userId).list();
        return Result.ok(userAddress);
    }

    @Override
    public Result pay(String orderNo) {
        OrderMsg a = new OrderMsg();
        a.setPayStatus("已支付");
        a.setPayTime(LocalDateTime.now());
        a.setOrderStatus("1");
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("order_no",orderNo);
        OrderMsg order = this.query().eq("order_no", orderNo).one();
        //根据订单编号查询商品编号和数量
        List<OrderItem> orderItems = orderMapper.selectGoodsIdAndGoodsCountByOrderId(order.getOrderId());
        for (int i = 0; i < orderItems.size(); i++) {
            GoodsInfo goods = goodsInfoService.query().eq("goods_id", orderItems.get(i).getGoodsId()).one();
            GoodsInfo goodsInfo = new GoodsInfo();
            goodsInfo.setStockNum(goods.getStockNum()-orderItems.get(i).getGoodsCount());
            QueryWrapper qq = new QueryWrapper();
            qq.eq("goods_id",orderItems.get(i).getGoodsId());
            boolean update = goodsInfoService.update(goodsInfo, qq);
        }
        boolean b = this.update(a, queryWrapper);
        if (b){
            return Result.ok("支付成功");
        }
        return Result.fail("支付失败");
    }

    @Override
    public Result delete(int id) {
        QueryWrapper<OrderMsg> queryWrapper =new QueryWrapper<>();
        queryWrapper.eq("order_id",id);
        OrderMsg a =new OrderMsg();
        a.setIsDeleted(1);
        boolean b = this.update(a,queryWrapper);
        if (!b){
            return Result.fail("订单删除失败，请稍后重试！");
        }
        return Result.ok("订单删除成功");
    }

    @Override
    public Result isOver(int id) {
        QueryWrapper<OrderMsg> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id",id);
        OrderMsg a = new OrderMsg();
        a.setOrderStatus("4");
        boolean b = this.update(a, queryWrapper);
        if (!b){
            return Result.fail("订单完成失败，请稍后重试！");
        }
        return Result.ok("订单完成成功");
    }

}
