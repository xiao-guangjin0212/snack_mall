package com.example.snack_mall.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.OrderItem;
import com.example.snack_mall.mapper.OrderItemMapper;
import com.example.snack_mall.service.IOrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements IOrderItemService {

    @Override
    public Result orderMsg(int id) {
        List<OrderItem> orderMsg = this.query().eq("order_id", id).list();
        return Result.ok(orderMsg);
    }
}
