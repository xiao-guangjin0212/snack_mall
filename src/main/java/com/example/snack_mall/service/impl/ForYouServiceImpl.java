package com.example.snack_mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.ForYou;
import com.example.snack_mall.mapper.ForYouMapper;
import com.example.snack_mall.service.IForYouService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.snack_mall.util.AdminHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Service
public class ForYouServiceImpl extends ServiceImpl<ForYouMapper, ForYou> implements IForYouService {

    @Resource
    private ForYouMapper forYouMapper;

    @Override
    public Result reg(ForYou forYou) {
        forYou.setAddPeople(AdminHolder.getUser().getAdminId());
        boolean save = this.save(forYou);
        if (!save){
            return Result.fail("商新增品失败，请稍后重试！");
        }
        return Result.ok("新增增品成功！");
    }

    @Override
    public Result edit(ForYou forYou) {
        boolean b = this.updateById(forYou);
        if (!b){
            return Result.fail("编辑商品失败，请稍后重试！");
        }
        return Result.ok("编辑商品成功！");
    }

    @Override
    public Result delete(int id) {
        QueryWrapper<ForYou> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("for_you_id",id);
        ForYou forYou = new ForYou();
        forYou.setIsDelete(1);
        boolean b = this.update(forYou,queryWrapper);
        if (!b){
            return Result.fail("删除商品失败，请稍后重试！");
        }
        return Result.ok("删除商品成功！");
    }

    @Override
    public Result deleteMany(List<Integer> ids) {
        QueryWrapper<ForYou> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("for_you_id",ids);
        ForYou forYou = new ForYou();
        forYou.setIsDelete(1);
        boolean b = this.update(forYou,queryWrapper);
        if (!b){
            return Result.fail("批量删除商品失败，请稍后重试！");
        }
        return Result.ok("批量删除商品成功！");
    }

    @Override
    public Result findPage(Page<ForYou> p) {
        Page<ForYou> page =  this.query().eq( "is_delete",0).page(p);
        return Result.ok(page);
    }

    @Override
    public Result forYouGoods() {
        List<ForYou> res = forYouMapper.forYouGoods();
        if (res == null){
            return Result.fail("为你推荐商品加载失败！");
        }
        return Result.ok(res);
    }
}
