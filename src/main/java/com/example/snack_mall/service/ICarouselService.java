package com.example.snack_mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.Carousel;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-02
 */
public interface ICarouselService extends IService<Carousel> {
    Result reg(Carousel carousel);

    Result del(int id);

    Result deleteMany(List<Integer> ids);

    Result edit(Carousel carousel);

    Result findPage(Page<Carousel> page);

    Result carouselUrl();
}
