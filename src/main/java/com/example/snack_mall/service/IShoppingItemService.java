package com.example.snack_mall.service;

import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.ShoppingItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface IShoppingItemService extends IService<ShoppingItem> {
    List<ShoppingItem> selectCart(int id);

    Result addCart(ShoppingItem shoppingItem);

    Result add(int cartItemId,int goodsCount);

    Result minus(int cartItemId, int goodsCount);

    Result delete(int id);

    Result deleteMany(List<Integer> ids);

    Result clear(Integer userId);
}
