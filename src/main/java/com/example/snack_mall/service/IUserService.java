package com.example.snack_mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.dto.UserDto;
import com.example.snack_mall.entity.Admin;
import com.example.snack_mall.entity.Carousel;
import com.example.snack_mall.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface IUserService extends IService<User> {

    Result reg(User user);
    //禁用
    Result lock(List <Integer> ids,Integer lockedFlag);
    //注销
    Result cancel(List <Integer> ids,Integer isDeleted);

    //修改
    Result edit(User user);
    //分页
    Result findPage(Page<User> page);


    Result login(User user,HttpSession session);

    Result send(User user);

    Result register(UserDto userDto);
}
