package com.example.snack_mall.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.GoodsCategory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface IGoodsCategoryService extends IService<GoodsCategory> {

    Result goodsByParent(int id);
    Result insert(GoodsCategory goodsCategory);

    Result updateCate(GoodsCategory goodsCategory);

    Result deleteCategory(int id);

    Result goodsCategory();

    Result findPage(Page<GoodsCategory> page);

    Result deleteMany(List<Integer> ids);

    Result selectLevel();

    Result selectFather(int id);

}
