package com.example.snack_mall.service;

import com.example.snack_mall.common.Result;
import com.example.snack_mall.dto.AdminDto;
import com.example.snack_mall.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface IAdminService extends IService<Admin> {

    Result login(Admin admin, HttpSession session);

    Result updateMsg(Admin admin);

    Result updatePassword(AdminDto adminDto);

    Result info(String token, HttpSession session);

}
