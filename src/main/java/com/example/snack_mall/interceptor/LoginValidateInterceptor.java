package com.example.snack_mall.interceptor;

import com.example.snack_mall.entity.Admin;
import com.example.snack_mall.util.AdminHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@Component
public class LoginValidateInterceptor implements HandlerInterceptor {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,Object handler){

        //获取session
        HttpSession session = request.getSession();
        //获取token
        String token = request.getHeader("token");
        //判断token是否为空，如果为空，=没有登录
        if (token == null || "".equals(token)){
            response.setStatus(401);
            return false;
        }
        //token来获取session当中的用户信息，不存在
        Object user = session.getAttribute(token);
        String s = stringRedisTemplate.opsForValue().get("login:user:" + token);
        if (user == null && s==null){
            response.setStatus(401);
            return false;
        }
        //用户信息存储到ThreadLocal当中
        if (user != null){
            AdminHolder.setUser((Admin) user);
        }

        //放行
        return true;
    }
}
