package com.example.snack_mall.config;

import com.example.snack_mall.interceptor.LoginValidateInterceptor;
import com.example.snack_mall.interceptor.RefreshTokenTTLInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class SnackMallMVCConfig implements WebMvcConfigurer {
    @Autowired
    private LoginValidateInterceptor loginValidateInterceptor;

    @Autowired
    private RefreshTokenTTLInterceptor refreshTokenTTLInterceptor;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("POST", "GET", "PUT", "OPTIONS", "DELETE")
//                .allowCredentials(true)
                .allowedHeaders("*")
                .maxAge(3600);
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        List<String> patterns = new ArrayList<>();
        patterns.add("/admin/login");
        patterns.add("/user/login");
        patterns.add("/user/send");
        patterns.add("/user/register");
        patterns.add("/for-you/forYouGoods");
        patterns.add("/carousel/carouselUrl");
        patterns.add("/hot/hotGoods");
        patterns.add("/new/newGoods");
        patterns.add("/goods-category/goodsCategory");
        patterns.add("/goods-info/goods/*");
        patterns.add("/goods-info/selectShopMsgById/*");
        patterns.add("/goods-info/selectGoodsBySearch/*");
        registry.addInterceptor(loginValidateInterceptor)
                .excludePathPatterns(patterns).order(2);

        registry.addInterceptor(refreshTokenTTLInterceptor)
                .excludePathPatterns("/user/login","/user/send","/user/register","/admin/login").order(1);
    }
}
