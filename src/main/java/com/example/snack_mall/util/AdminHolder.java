package com.example.snack_mall.util;

import com.example.snack_mall.entity.Admin;

public class AdminHolder {
    private static final ThreadLocal<Admin> TL = new ThreadLocal<>();
    public static void setUser(Admin user){
        TL.set(user);
    }
    public static Admin getUser(){
        return TL.get();
    }
}
