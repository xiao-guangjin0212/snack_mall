package com.example.snack_mall.util;

import com.example.snack_mall.entity.User;

public class UserHolder {
    private static final ThreadLocal<User> TL = new ThreadLocal<>();
    public static void setUser(User user){
        TL.set(user);
    }
    public static User getUser(){
        return TL.get();
    }
}
