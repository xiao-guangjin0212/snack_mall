package com.example.snack_mall;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.example.snack_mall.mapper")
public class SnackMallApplication {

    public static void main(String[] args) {
        SpringApplication.run(SnackMallApplication.class, args);
    }

}
