package com.example.snack_mall.vo;

import com.example.snack_mall.entity.GoodsInfo;
import com.example.snack_mall.entity.OrderMsg;
import lombok.Data;

import java.util.List;

@Data
public class OrderMsgVo {
    private List<GoodsInfo> goodsInfo;
    private List<OrderMsg> orderMsg;
}
