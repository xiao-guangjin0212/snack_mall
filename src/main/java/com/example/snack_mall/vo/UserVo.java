package com.example.snack_mall.vo;

import com.example.snack_mall.entity.User;
import lombok.Data;

@Data
public class UserVo {
    private User user;
    private String token;
}
