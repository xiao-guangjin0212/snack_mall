package com.example.snack_mall.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class Carousel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 首页轮播图主键id
     */
      @TableId(value = "carousel_id", type = IdType.AUTO)
      @ApiModelProperty(value = "轮播图编号", example = "22")
    private Integer carouselId;

    /**
     * 轮播图
     */
    @ApiModelProperty(value = "轮播图地址", example = "http://dsaas.com.1.jpg")
    private String carouselUrl;

    /**
     * 点击后的跳转地址(默认不跳转)
     */
    @ApiModelProperty(value = "轮播图跳转地址", example = "http://8080.curousel.aaa")
    private String redirectUrl;

    /**
     * 排序值(字段越大越靠前)
     */
    @ApiModelProperty(value = "轮播图排序值", example = "40")
    private Integer carouselRank;

    /**
     * 删除标识字段(0-未删除 1-已删除)
     */
    @ApiModelProperty(value = "轮播图是否删除", example = "0")
    private Integer isDeleted;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "轮播图添加时间")
    private LocalDateTime addtime;

    /**
     * 创建者id
     */
    @ApiModelProperty(value = "轮播图添加人", example = "22")
    private Integer addPeople;


}
