package com.example.snack_mall.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class ShoppingItem implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 购物项主键id
     */

      @TableId(value = "cart_item_id", type = IdType.AUTO)
      @ApiModelProperty(value = "购物项主键",example = "1")
    private Integer cartItemId;

    /**
     * 用户主键id
     */
    @ApiModelProperty(value = "用户主键编号",example = "1")
    private Integer userId;

    /**
     * 关联商品id
     */
    @ApiModelProperty(value = "关联商品编号",example = "1")
    private Integer goodsId;

    /**
     * 数量(最大为5)
     */
    @ApiModelProperty(value = "商品数量",example = "1")
    private Integer goodsCount;

    /**
     * 删除标识字段(0-未删除 1-已删除)
     */
    @ApiModelProperty(value = "是否删除",example = "0")
    private Integer isDeleted;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "购物项创建时间")
    private LocalDateTime addtime;


    @TableField(exist = false)
    @ApiModelProperty(value = "商品信息")
    private GoodsInfo goodsInfo;

}
