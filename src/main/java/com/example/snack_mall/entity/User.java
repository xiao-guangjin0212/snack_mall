package com.example.snack_mall.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户主键id
     */
      @TableId(value = "user_id", type = IdType.AUTO)
      @ApiModelProperty(value = "用户主键编号",example = "1")
    private Integer userId;

    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称",example = "小明")
    private String nickName;

    /**
     * 登陆名称(默认为手机号)
     */
    @ApiModelProperty(value = "登录名称",example = "默认为手机号1223554688")
    private String loginName;

    /**
     * MD5加密后的密码
     */
    @ApiModelProperty(value = "MD5加密后的密码",example = "d1sad1513a5d3a6dw1d6wa")
    private String passwordMd5;

    /**
     * 个性签名
     */
    @ApiModelProperty(value = "个性签名",example = "好烦啊")
    private String introduceSign;

    /**
     * 收货地址
     */
    @ApiModelProperty(value = "收货地址",example = "大连市甘井子区")
    private String address;

    /**
     * 注销标识字段(0-正常 1-已注销)
     */
    @ApiModelProperty(value = "是否注销",example = "0")
    private Integer isDeleted;

    /**
     * 锁定标识字段(0-未锁定 1-已锁定)
     */
    @ApiModelProperty(value = "是否锁定",example = "0")
    private Integer lockedFlag;

    /**
     * 注册时间
     */
    @ApiModelProperty(value = "注册时间")
    private LocalDateTime addtime;


}
