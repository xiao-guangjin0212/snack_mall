package com.example.snack_mall.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class New implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 新品上线主键
     */
    @TableId(value = "new_id", type = IdType.AUTO)
    @ApiModelProperty(value = "新品上线主键",example = "1")
      private Integer newId;

    /**
     * 新品名称
     */
    @ApiModelProperty(value = "新品名称",example = "瓜子")
    private String newName;

    /**
     * 商品编号
     */
    @ApiModelProperty(value = "商品编号",example = "1")
    private Integer goodsId;

    /**
     * 商品地址
     */
    @ApiModelProperty(value = "商品地址",example = "http:aliyun.com.1.jpg")
    private String goodsUrl;

    /**
     * 排序值
     */
    @ApiModelProperty(value = "新品排序值",example = "66")
    private Integer newRank;

    /**
     * 是否删除0-未删除 1-已删除
     */
    @ApiModelProperty(value = "是否删除",example = "0")
    private Integer isDelete;

    /**
     * 添加时间
     */
    @ApiModelProperty(value = "添加时间")
    private LocalDateTime addtime;

    /**
     * 添加人
     */
    @ApiModelProperty(value = "添加人",example = "1")
    private Integer addPeople;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人",example = "1")
    private Integer updatePeople;

    @TableField(exist = false)
    @ApiModelProperty(value = "商品实体类")
    private GoodsInfo goodsInfo;
}
