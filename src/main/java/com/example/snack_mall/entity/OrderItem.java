package com.example.snack_mall.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class OrderItem implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单关联购物项主键id
     */

      @TableId(value = "order_item_id", type = IdType.AUTO)
      @ApiModelProperty(value = "订单关联购物项主键",example = "1")
    private Integer orderItemId;

    /**
     * 订单主键id
     */
    @ApiModelProperty(value = "订单主键编号",example = "1")
    private Integer orderId;

    /**
     * 关联商品id
     */
    @ApiModelProperty(value = "关联商品编号",example = "1")
    private Integer goodsId;

    /**
     * 下单时商品的名称(订单快照)
     */
    @ApiModelProperty(value = "下单时商品名称",example = "瓜子")
    private String goodsName;

    /**
     * 下单时商品的主图(订单快照)
     */
    @ApiModelProperty(value = "下单时商品主图",example = "http://aliyun.com.1.jpg")
    private String goodsCoverImg;

    /**
     * 下单时商品的价格(订单快照)
     */
    @ApiModelProperty(value = "下单时商品价格",example = "20")
    private Integer sellingPrice;

    /**
     * 数量(订单快照)
     */
    @ApiModelProperty(value = "下单数量",example = "1")
    private Integer goodsCount;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "订单商品关联项创建时间")
    private LocalDateTime addtime;


    @TableField(exist = false)
    @ApiModelProperty(value = "订单信息")
    private List<OrderMsg> orderMsgs;


    @TableField(exist = false)
    @ApiModelProperty(value = "商品信息")
    private List<GoodsInfo> goodsInfos;
}
