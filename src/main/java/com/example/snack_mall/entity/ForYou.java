package com.example.snack_mall.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class ForYou implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 为你推荐主键
     */
    @TableId(value = "for_you_id", type = IdType.AUTO)
    @ApiModelProperty(value = "为你推荐主键", example = "1")
      private Integer forYouId;

    /**
     * 为你推荐名称
     */
    @ApiModelProperty(value = "为你推荐名称", example = "鱿鱼丝")
    private String forYouName;

    /**
     * 商品编号
     */
    @ApiModelProperty(value = "商品编号", example = "22")
    private Integer goodsId;

    /**
     * 商品地址
     */
    @ApiModelProperty(value = "商品地址", example = "http://aliyun.com.1.jpg")
    private String goodsUrl;

    /**
     * 排序值
     */
    @ApiModelProperty(value = "为你推荐排序值", example = "33")
    private Integer forYouRank;

    /**
     * 是否删除0-未删除，1-删除
     */
    @ApiModelProperty(value = "是否被删除", example = "0")
    private Integer isDelete;

    /**
     * 添加时间
     */
    @ApiModelProperty(value = "为你推荐添加时间")
    private LocalDateTime addtime;

    /**
     * 添加人
     */
    @ApiModelProperty(value = "轮为你推荐添加人", example = "2")
    private Integer addPeople;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "问你推荐修改时间")
    private LocalDateTime updateTime;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "为你推荐修改人", example = "2")
    private Integer updatePeople;


    @TableField(exist = false)
    @ApiModelProperty(value = "商品实体类")
    private GoodsInfo goodsInfo;
}
