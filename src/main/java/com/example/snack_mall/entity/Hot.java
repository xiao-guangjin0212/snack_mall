package com.example.snack_mall.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class Hot implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 首页配置项主键id
     */
      @TableId(value = "hot_id", type = IdType.AUTO)
      @ApiModelProperty(value = "热销商品主键",example = "1")
    private Long hotId;

    /**
     * 显示字符(配置搜索时不可为空，其他可为空)
     */
    @ApiModelProperty(value = "热销商品名",example = "瓜子")
    private String hotName;

    /**
     * 1-搜索框热搜 2-搜索下拉框热搜 3-(首页)热销商品 4-(首页)新品上线 5-(首页)为你推荐
     */
    @ApiModelProperty(value = "热销商品排序值",example = "40")
    private Integer hotType;

    /**
     * 商品id 默认为0
     */
    @ApiModelProperty(value = "商品编号",example = "1")
    private Long goodsId;

    /**
     * 点击后的跳转地址(默认不跳转)
     */
    @ApiModelProperty(value = "点击后跳转地址",example = "http://8080.hot.aaa")
    private String goodsUrl;

    /**
     * 排序值(字段越大越靠前)
     */
    @ApiModelProperty(value = "热销商品排序值",example = "22")
    private Integer hotRank;

    /**
     * 删除标识字段(0-未删除 1-已删除)
     */
    @ApiModelProperty(value = "是否删除",example = "0")
    private Integer isDeleted;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "添加时间")
    private LocalDateTime addtime;

    /**
     * 创建者id
     */
    @ApiModelProperty(value = "添加人编号",example = "1")
    private Integer addPeople;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "商修改人",example = "1")
    private Integer updatePeople;


    @ApiModelProperty(value = "新品上线对象")
    @TableField(exist = false)
    private GoodsInfo goodsInfo;
}
