package com.example.snack_mall.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class GoodsCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分类id
     */
      @TableId(value = "category_id", type = IdType.AUTO)
      @ApiModelProperty(value = "分类主键", example = "1")
    private Integer categoryId;

    /**
     * 分类级别(1-一级分类 2-二级分类 3-三级分类)
     */
    @ApiModelProperty(value = "分类级别", example = "1")
    private Integer categoryLevel;

    /**
     * 父分类id
     */
    @ApiModelProperty(value = "父分类编号", example = "1")
    private Integer parentId;

    /**
     * 分类名称
     */
    @ApiModelProperty(value = "分类名称", example = "干果")
    private String categoryName;

    /**
     * 排序值(字段越大越靠前)
     */
    @ApiModelProperty(value = "分类主键", example = "1")
    private Integer categoryRank;

    /**
     * 删除标识字段(0-未删除 1-已删除)
     */
    @ApiModelProperty(value = "是否删除", example = "0")
    private Integer isDeleted;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "添加时间")
    private LocalDateTime addtime;

    /**
     * 创建者id
     */
    @ApiModelProperty(value = "添加人",example = "1")
    private Integer addPeople;

    @TableField(exist = false)
    private List<GoodsCategory> children;
}
