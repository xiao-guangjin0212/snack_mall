package com.example.snack_mall.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import com.example.snack_mall.mapper.OrderMapper;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class OrderMsg implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单表主键id
     */
      @TableId(value = "order_id", type = IdType.AUTO)
      @ApiModelProperty(value = "订单表主键",example = "1")
    private Integer orderId;

    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号",example = "3336666555")
    private String orderNo;

    /**
     * 用户主键id
     */
    @ApiModelProperty(value = "关联用户主键",example = "1")
    private Integer userId;

    /**
     * 订单总价
     */
    @ApiModelProperty(value = "订单总价",example = "100")
    private Integer totalPrice;

    /**
     * 支付状态:0.未支付,1.支付成功,-1:支付失败
     */
    @ApiModelProperty(value = "支付状态",example = "1")
    private String payStatus;

    /**
     * 0.无 1.支付宝支付 2.微信支付
     */
    @ApiModelProperty(value = "支付方式",example = "1")
    private String payType;

    /**
     * 支付时间
     */
    @ApiModelProperty(value = "支付时间")
    private LocalDateTime payTime;

    /**
     * 订单状态:0.待支付 1.已支付 2.配货完成 3:出库成功 4.交易成功 -1.手动关闭 -2.超时关闭 -3.商家关闭
     */
    @ApiModelProperty(value = "订单状态",example = "1")
    private String orderStatus;

    /**
     * 订单body
     */
    @ApiModelProperty(value = "订单信息")
    private String extraInfo;

    /**
     * 收货人姓名
     */
    @ApiModelProperty(value = "收货人姓名",example = "小明")
    private String userName;

    /**
     * 收货人手机号
     */
    @ApiModelProperty(value = "收货人手机号",example = "1333999888")
    private String userPhone;

    /**
     * 收货人收货地址
     */
    @ApiModelProperty(value = "收货人地址",example = "大连市甘井子区")
    private String userAddress;

    /**
     * 删除标识字段(0-未删除 1-已删除)
     */
    @ApiModelProperty(value = "是否删除",example = "0")
    private Integer isDeleted;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "退订单创建时间")
    private LocalDateTime addtime;

    @TableField(exist = false)
    private Integer current;
    @TableField(exist = false)
    private Integer size;
    @TableField(exist = false)
    private List<GoodsInfo> goodsInfo;
    @TableField(exist = false)
    private List<OrderItem> orderItem;

    @TableField(exist = false)
    private Integer goodsCount;
}
