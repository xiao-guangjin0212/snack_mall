package com.example.snack_mall.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class Admin implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 管理员id
     */
      @TableId(value = "admin_id", type = IdType.AUTO)
      @ApiModelProperty(value = "管理员编号", example = "1")
    private Integer adminId;

    /**
     * 管理员登陆名称
     */
    @ApiModelProperty(value = "管理员登录账号", example = "aa123")
    private String adminAccount;

    /**
     * 管理员登陆密码
     */
    @ApiModelProperty(value = "管理员登录密码", example = "bb123")
    private String adminPassword;

    /**
     * 管理员显示昵称
     */
    @ApiModelProperty(value = "管理员显示昵称", example = "小明")
    private String adminName;

    /**
     * 是否锁定 0未锁定 1已锁定无法登陆
     */
    @ApiModelProperty(value = "管理员是否被锁定", example = "0")
    private Integer locked;


}
