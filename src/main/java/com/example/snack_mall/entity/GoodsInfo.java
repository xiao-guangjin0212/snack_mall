package com.example.snack_mall.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel
public class GoodsInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 商品表主键id
     */
      @TableId(value = "goods_id", type = IdType.AUTO)
      @ApiModelProperty(value = "商品表主键",example = "1")
    private Integer goodsId;

    /**
     * 商品名
     */
    @ApiModelProperty(value = "商品名",example = "瓜子")
    private String goodsName;

    /**
     * 商品简介
     */
    @ApiModelProperty(value = "商品简介",example = "不好吃")
    private String goodsIntro;

    /**
     * 关联分类id
     */
    @ApiModelProperty(value = "关联分类表主键",example = "1")
    private Integer goodsCategoryId;

    /**
     * 商品主图
     */
    @ApiModelProperty(value = "商品主图",example = "http:aliyu.com.1.jpg")
    private String goodsCoverImg;

    /**
     * 商品轮播图
     */
    @ApiModelProperty(value = "商品轮播图",example = "http:aliyu.com.1.jpg")
    private String goodsCarousel;

    /**
     * 商品详情
     */
    @ApiModelProperty(value = "商品详情",example = "产地：....")
    private String goodsDetailContent;

    /**
     * 商品价格
     */
    @ApiModelProperty(value = "商品价格",example = "25")
    private Integer originalPrice;

    /**
     * 商品实际售价
     */
    @ApiModelProperty(value = "商品实际售价",example = "20")
    private Integer sellingPrice;

    /**
     * 商品库存数量
     */
    @ApiModelProperty(value = "商品库存",example = "33")
    private Integer stockNum;

    /**
     * 商品标签
     */
    @ApiModelProperty(value = "商品标签",example = "干果")
    private String tag;

    /**
     * 商品上架状态 0-下架 1-上架
     */
    @ApiModelProperty(value = "商品上架状态",example = "0")
    private Integer goodsSellStatus;

    /**
     * 添加者主键id
     */
    @ApiModelProperty(value = "添加人主键编号",example = "1")
    private Integer addPeople;

    /**
     * 商品添加时间
     */
    @ApiModelProperty(value = "商品添加时间")
    private LocalDateTime addtime;

    @ApiModelProperty(value = "热销商品对象")
    @TableField(exist = false)
    private Hot hot;

    @ApiModelProperty(value = "新品上线对象")
    @TableField(exist = false)
    private New news;



    @TableField(exist = false)
    private OrderMsg orderMsg;
    @TableField(exist = false)
    private OrderItem orderItem;

    @TableField(exist = false)
    private Integer goodsCount;
}
