package com.example.snack_mall.mapper;

import com.example.snack_mall.entity.GoodsInfo;
import com.example.snack_mall.entity.ShoppingItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface ShoppingItemMapper extends BaseMapper<ShoppingItem> {

    List<ShoppingItem> selectCart(int id);

}
