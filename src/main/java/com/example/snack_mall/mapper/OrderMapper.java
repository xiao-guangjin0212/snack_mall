package com.example.snack_mall.mapper;

import com.example.snack_mall.entity.OrderItem;
import com.example.snack_mall.entity.OrderMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.snack_mall.entity.ShoppingItem;
import io.swagger.annotations.Authorization;
import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Description;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderMsg> {

    List<OrderMsg> findPage();


    List<OrderItem> selectGoodsIdAndGoodsCountByOrderId(Integer orderId);
}
