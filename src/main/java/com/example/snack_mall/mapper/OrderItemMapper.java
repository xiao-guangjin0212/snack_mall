package com.example.snack_mall.mapper;

import com.example.snack_mall.entity.GoodsInfo;
import com.example.snack_mall.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.snack_mall.entity.OrderMsg;
import com.example.snack_mall.vo.OrderMsgVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface OrderItemMapper extends BaseMapper<OrderItem> {

    List<OrderMsgVo> selectOrderByUserId(Integer userId);


}
