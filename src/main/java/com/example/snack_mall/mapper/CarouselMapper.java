package com.example.snack_mall.mapper;

import com.example.snack_mall.entity.Carousel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface CarouselMapper extends BaseMapper<Carousel> {

}
