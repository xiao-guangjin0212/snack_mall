package com.example.snack_mall.mapper;

import com.example.snack_mall.entity.ForYou;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@Mapper
public interface ForYouMapper extends BaseMapper<ForYou> {


    List<ForYou> forYouGoods();
}
