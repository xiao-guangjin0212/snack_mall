package com.example.snack_mall.mapper;

import com.example.snack_mall.entity.GoodsInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
public interface GoodsInfoMapper extends BaseMapper<GoodsInfo> {

}
