package com.example.snack_mall.dto;

import com.example.snack_mall.entity.User;
import lombok.Data;

@Data
public class UserDto {
    private User user;
    private Integer code;
}
