package com.example.snack_mall.dto;

import com.example.snack_mall.entity.Admin;
import lombok.Data;

@Data
public class AdminDto {

    private String oldAdminPassword;

    private String newAdminPassword;
}
