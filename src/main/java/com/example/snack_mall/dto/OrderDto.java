package com.example.snack_mall.dto;

import com.example.snack_mall.entity.OrderMsg;
import com.example.snack_mall.entity.ShoppingItem;
import lombok.Data;

import java.util.List;

@Data
public class OrderDto {
    private OrderMsg orderMsg;
    private List<ShoppingItem> shoppingItems;
}
