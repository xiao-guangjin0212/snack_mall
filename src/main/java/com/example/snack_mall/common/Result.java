package com.example.snack_mall.common;

import lombok.Data;


@Data
public class Result<T> {
    private int status;
    private String msg;
    private T data;

    public static Result ok(String msg){
        Result result = new Result();
        result.setStatus(200);
        result.setMsg(msg);
        return result;
    }
    public static Result ok(Object data){
        Result result = new Result();
        result.setStatus(200);
        result.setData(data);
        return result;
    }
    public static Result ok(String msg,Object data){
        Result result = new Result();
        result.setStatus(200);
        result.setMsg(msg);
        result.setData(data);
        return result;
    }
    public static Result fail(String msg){
        Result result = new Result();
        result.setStatus(-1);
        result.setMsg(msg);
        return result;
    }
    public static Result fail(Object data){
        Result result = new Result();
        result.setStatus(-1);
        result.setData(data);
        return result;
    }
    public static Result fail(String msg,Object data){
        Result result = new Result();
        result.setStatus(-1);
        result.setMsg(msg);
        result.setData(data);
        return result;
    }
}
