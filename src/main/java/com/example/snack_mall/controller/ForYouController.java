package com.example.snack_mall.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.Carousel;
import com.example.snack_mall.entity.ForYou;
import com.example.snack_mall.service.IForYouService;
import com.example.snack_mall.service.impl.ForYouServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@RestController
@RequestMapping("/for-you")
@Api(tags = "为你推荐")
public class ForYouController {

    @Resource
    private IForYouService forYouService;

    //新增
    @PostMapping("/reg")
    @ApiOperation(value = "为你推荐商品新增")
    public Result reg(@RequestBody ForYou forYou) {
        return forYouService.reg(forYou);
    }

    //删除
    @DeleteMapping("/del/{id}")
    @ApiOperation(value = "为你推荐商品删除")
    public Result del(@PathVariable int id){
        return Result.ok(forYouService.delete(id));
    }

    //批量删除
    @PostMapping("/deleteMany")
    @ApiOperation(value = "为你推荐商品批量删除")
    public Result batchDel(@RequestBody List<Integer> ids){
        return Result.ok(forYouService.deleteMany(ids));
    }

    @ApiOperation(value = "为你推荐编辑")
    @PutMapping("/edit")
    public Result edit(@RequestBody ForYou forYou) {
        return forYouService.edit(forYou);
    }

    @ApiOperation(value = "为你推荐分页查询")
    @GetMapping("/findPage")
    public Result findPage(Page<ForYou> page){
        return forYouService.findPage(page);
    }

    @ApiOperation(value = "查询所有为你推荐的商品信息")
    @GetMapping("/forYouGoods")
    public Result forYouGoods(){
        return forYouService.forYouGoods();
    }
}

