package com.example.snack_mall.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.dto.UserDto;
import com.example.snack_mall.entity.User;
import com.example.snack_mall.service.IUserService;
import com.example.snack_mall.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@RestController
@RequestMapping("/user")
@Api(tags = "会员管理")
public class UserController {
    @Resource
    IUserService userService;

    @ApiOperation(value = "会员注册")
    @PostMapping("/reg")
    public Result reg(@RequestBody User user){
        return userService.reg(user);
    }


    @ApiOperation(value = "会员注销状态")
    @PutMapping("/cancel/{isDeleted}")
    @ApiImplicitParam(name = "isDeleted",value = "注销状态")
    public Result cancel(@RequestBody List<Integer> ids,@PathVariable Integer isDeleted){
        return userService.cancel(ids,isDeleted);
    }



    @ApiOperation(value = "会员禁用状态")
    @PutMapping("/lock/{lockedFlag}")
    @ApiImplicitParam(name = "lockedFlag",value = "禁用状态")
    public Result lock(@RequestBody List<Integer> ids,@PathVariable Integer lockedFlag){
        return userService.lock(ids,lockedFlag);
    }



    @ApiOperation(value = "会员编辑")
    @PutMapping("/edit")
    public Result edit(@RequestBody User user){
        return userService.edit(user);
    }


    @ApiOperation(value = "会员分页查询")
    @GetMapping("/findPage")
    public Result findPage(Page<User> page){
        return userService.findPage(page);
    }


    //用户登录
    @ApiOperation(value = "用户登录")
    @GetMapping("/login")
    public Result login(User user, HttpSession session){
        return userService.login(user, session);
    }

    @ApiOperation(value = "发送验证码")
    @GetMapping("/send")
    public Result send(User user){
        return userService.send(user);
    }

    @ApiOperation(value = "注册")
    @PostMapping("/register")
    public Result register(@RequestBody UserDto userDto){
        return userService.register(userDto);
    }
}

