package com.example.snack_mall.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.dto.SearchDto;
import com.example.snack_mall.entity.GoodsInfo;
import com.example.snack_mall.entity.New;
import com.example.snack_mall.service.IGoodsInfoService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.GoodsInfo;
import com.example.snack_mall.service.IGoodsInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@RestController
@RequestMapping("/goods-info")
@Api(tags = "商品管理")
public class GoodsInfoController {
    @Resource
    private IGoodsInfoService goodsInfoService;

    @ApiOperation(value = "商品管理添加")
    @PostMapping("/reg")
    public Result reg(@RequestBody GoodsInfo goodsInfo) {
        return goodsInfoService.reg(goodsInfo);
    }

    @ApiOperation(value = "商品管理编辑")
    @PostMapping("/edit")
    public Result edit(@RequestBody GoodsInfo goodsInfo) {
        return goodsInfoService.edit(goodsInfo);
    }

    @ApiOperation(value = "商品信息编辑")
    @PutMapping("updateGoodsInfo")
    public Result updateGoodsInfo(@RequestBody GoodsInfo goodsInfo) {
        return goodsInfoService.updateGoodsInfo(goodsInfo);
    }

    @ApiOperation(value = "修改商品上架状态")
    @PutMapping("/editGoodsSellStatus")
    public Result editStatus( @RequestBody GoodsInfo goodsInfo) {
        return goodsInfoService.editGoodsSellStatus(goodsInfo);
    }

    @ApiOperation(value = "商品管理分页查询")
    @GetMapping("/findPage")
    public Result findPage(Page<GoodsInfo> page){
        return goodsInfoService.findPage(page);
    }

    @ApiOperation(value =  "根据商品分类编号查询商品信息")
    @GetMapping("/goodsMsg/{id}")
    public Result goodsMsg(@PathVariable int id){
        return goodsInfoService.goodsMsg(id);
    }

    @ApiOperation(value = "查询所有商品")
    @GetMapping("/selectShop")
    public Result selectShop(){
        return goodsInfoService.selectShop();
    }

    @ApiOperation(value = "根据商品编号查询商品信息")
    @DeleteMapping("/selectShopMsgById/{id}")
    public Result selectShopMsgById(@PathVariable int id){
        return goodsInfoService.selectShopMsgById(id);
    }

    @ApiOperation(value = "根据分类编号查询商品信息")
    @GetMapping("/goods/{id}")
    public Result goodsByCategoryId(@PathVariable int id){
        return goodsInfoService.goodsByCategoryId(id);
    }

    @ApiOperation(value = "模糊查询商品信息")
    @GetMapping("/selectGoodsBySearch/{msg}")
    public Result selectGoodsBySearch(@PathVariable String msg){
        return goodsInfoService.selectGoodsBySearch(msg);
    }
}

