package com.example.snack_mall.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.service.IGoodsCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.GoodsCategory;
import com.example.snack_mall.entity.Hot;
import com.example.snack_mall.service.IGoodsCategoryService;
import com.example.snack_mall.service.IHotService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@RestController
@Api(tags = "分类管理")
@RequestMapping("/goods-category")
public class GoodsCategoryController {
    @Resource
    private IGoodsCategoryService goodsCategoryService;

    @ApiOperation(value = "分类分页查询")
    @GetMapping("/findPage")
    public Result findPage(Page<GoodsCategory> page){
        return goodsCategoryService.findPage(page);
    }


    @ApiOperation(value = "根据父类查询所有子类")
    @GetMapping("/goodsByParent")
    public Result goodsByParent(int id){
        return goodsCategoryService.goodsByParent(id);
    }

    @ApiOperation(value = "新增分类")
    @PostMapping("/insert")
    public Result insert(@RequestBody GoodsCategory goodsCategory){
        return Result.ok(goodsCategoryService.save(goodsCategory));
    }


    @ApiOperation(value = "修改分类")
    @PutMapping("/update")
    public Result update(@RequestBody GoodsCategory goodsCategory){
        return goodsCategoryService.updateCate(goodsCategory);
    }


    @ApiOperation(value = "分类删除")
    @DeleteMapping("/delete/{id}")
    @ApiImplicitParam(name = "id",value = "分类编号")
    public Result delete(@PathVariable("id") int id){
//        return Result.ok(goodsCategoryService.deleteCategory(id));
        return goodsCategoryService.deleteCategory(id);
    }

    @ApiOperation(value = "分类批量删除")
    @DeleteMapping("/del")
    @ApiImplicitParam(name = "id",value = "分类编号")
    public Result deleteMany(@RequestBody List<Integer> ids){
        return goodsCategoryService.deleteMany(ids);
    }

    @ApiOperation(value = "商品分类查询")
    @GetMapping("/goodsCategory")
    public Result goodsCategory(){
        return goodsCategoryService.goodsCategory();
    }

    @ApiOperation(value = "查询所有一级二级分类")
    @GetMapping("/selectLevel")
    public Result selectLevel(){
        return goodsCategoryService.selectLevel();
    }

    @ApiOperation(value = "根据分类编号查询分类信息")
    @DeleteMapping("/selectFather/{id}")
    public Result selectFather(@PathVariable int id){
        return goodsCategoryService.selectFather(id);
    }


}

