package com.example.snack_mall.controller;


import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.Carousel;
import com.example.snack_mall.entity.Hot;
import com.example.snack_mall.service.IHotService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@RestController
@RequestMapping("/hot")
@Api(tags = "热销商品")
public class HotController {
    @Resource
    private IHotService hotService;

    @ApiOperation(value = "热销商品分页查询")
    @GetMapping("/findPage")
    public Result findPage(Page<Hot> page){
        return hotService.findPage(page);
    }


//    @ApiOperation(value = "新增商品")
//    @PostMapping("/insert")
//    public Result insert(@RequestBody Hot hot){
//        return Result.ok(hotService.save(hot));
//    }
    @ApiOperation(value = "添加商品")
    @PostMapping("/insert")
    public Result insert(@RequestBody Hot hot){
        return hotService.insert(hot);
    }


    @ApiOperation(value = "修改商品")
    @PutMapping("/update")
    public Result updateHot(@RequestBody Hot hot){
        return hotService.updateHot(hot);
    }


    @ApiOperation(value = "热销商品删除")
    @DeleteMapping("/delete/{id}")
    @ApiImplicitParam(name = "id",value = "热销商品编号")
    public Result delete(@PathVariable("id") int id){
        return Result.ok(hotService.removeById(id));
    }


    @ApiOperation(value = "热销商品批量删除")
    @PostMapping("/del")
    @ApiImplicitParam(name = "id",value = "热销商品编号")
    public Result deleteMany(@RequestBody List<Integer> ids){
        return hotService.deleteMany(ids);
    }

    @ApiModelProperty(value = "热销商品信息")
    @GetMapping("/hotGoods")
    public Result hotGoods(){
        return hotService.hotGoods();
    }
}

