package com.example.snack_mall.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.dto.OrderDto;
import com.example.snack_mall.entity.OrderItem;
import com.example.snack_mall.entity.OrderMsg;
import com.example.snack_mall.entity.ShoppingItem;
import com.example.snack_mall.service.IOrderItemService;
import com.example.snack_mall.service.IOrderService;
import com.example.snack_mall.vo.OrderMsgVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@RestController
@RequestMapping("/order")
@Api(tags = "订单表")
public class OrderController {
    @Resource
    private IOrderService orderService;
    @Resource
    private IOrderItemService orderItemService;

    @ApiOperation(value = "订单新增")
    @PostMapping("/reg")
    public Result reg(@RequestBody OrderDto orderDto) {
        return orderService.reg(orderDto);
    }

    @ApiOperation(value = "订单批量删除")
    @PostMapping("/deleteMany")
    public Result deleteMany(@RequestBody List<Integer> ids) {
        return orderService.del(ids);
    }

    @ApiOperation(value = "订单修改")
    @PutMapping("/edit")
    public Result edit(@RequestBody OrderMsg orderMsg) {
        return orderService.edit(orderMsg);
    }

    @ApiOperation(value = "配货完成")
    @PostMapping("/distribute")
    public Result distribute(@RequestBody List<Integer> ids) {
        return orderService.dis(ids);
    }

    @ApiOperation(value = "出库")
    @PostMapping("/outbound")
    public Result outbound(@RequestBody List<Integer> ids) {
        return orderService.out(ids);
    }

    @ApiOperation(value = "订单分页查询")
    @GetMapping("findPage")
    public Result findPage(Page<OrderMsg> page) {
        return orderService.findPage(page);
    }

    @ApiOperation(value = "查看订单信息")
    @GetMapping("/orderMsg/{id}")
    @ApiImplicitParam(name = "id", value = "订单编号")
    public Result orderMsg(@PathVariable int id) {
        return orderItemService.orderMsg(id);
    }

    @ApiOperation(value = "查看收货人信息")
    @GetMapping("/orderPeople/{id}")
    @ApiImplicitParam(name = "id", value = "订单编号")
    public Result orderPeople(@PathVariable int id) {
        return orderService.orderPeople(id);
    }


    //前台
    @ApiOperation(value = "我的订单分页查询")
    @PostMapping("/page/{userId}")
    public Result Page(@RequestBody Page<OrderMsgVo> page, @PathVariable Integer userId) {
        return orderService.Page(page,userId);
    }
    @ApiOperation(value = "我的订单删除")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable int id){
        return orderService.delete(id);
    }

    @ApiOperation(value = "我的订单分页查询")
    @GetMapping("/address")
    public Result address(Integer userId) {
        return orderService.address(userId);
    }

    @ApiOperation(value = "支付")
    @PostMapping("/pay")
    public Result pay(String orderNo) {
        return orderService.pay(orderNo);
    }

    @ApiOperation(value = "订单完成")
    @PutMapping("/isOver/{id}")
    public Result isOver(@PathVariable int id){
        return orderService.isOver(id);
    }
}

