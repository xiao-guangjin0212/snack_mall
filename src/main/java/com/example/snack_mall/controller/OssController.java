package com.example.snack_mall.controller;

import com.example.snack_mall.common.Result;
import com.example.snack_mall.service.OssService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/oss")
public class OssController {
    @Resource
    OssService ossService;

    @PostMapping("/upload")
    @ResponseBody
    public Result upload(MultipartFile multipartFile) throws IOException {
        String url = ossService.addHeadImage(multipartFile);
        return Result.ok("", url);
    }
}
