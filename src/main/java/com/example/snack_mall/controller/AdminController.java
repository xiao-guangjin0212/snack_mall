package com.example.snack_mall.controller;


import com.example.snack_mall.common.Result;
import com.example.snack_mall.dto.AdminDto;
import com.example.snack_mall.entity.Admin;
import com.example.snack_mall.service.IAdminService;
import com.example.snack_mall.util.AdminHolder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.http.cookie.Cookie;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@RestController
@RequestMapping("/admin")
@Api(tags = "管理员")
public class AdminController {
    @Resource
    private IAdminService adminService;
    @ApiOperation(value = "登录")
    @GetMapping("/login")
    public Result login( Admin admin, HttpSession session){
        return adminService.login(admin,session);
    }

    @ApiOperation(value = "修改登录信息")
    @PutMapping("/updateMsg")
    public Result updateMsg(@RequestBody Admin admin){
        return adminService.updateMsg(admin);
    }

    @ApiOperation(value = "修改密码")
    @PutMapping("/updatePassword")
    public Result updatePassword(@RequestBody AdminDto adminDto){
        return adminService.updatePassword(adminDto);
    }

    @ApiOperation(value = "获取用户信息")
    @GetMapping("/info")
    public Result info(){
        return Result.ok(AdminHolder.getUser());
    }

    @ApiOperation(value = "登出")
    @GetMapping("/logout")
    public Result logout(HttpSession session){
        session.invalidate();
        return Result.ok("session已被清空");
    }
}

