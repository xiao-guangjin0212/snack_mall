package com.example.snack_mall.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.ForYou;
import com.example.snack_mall.entity.New;
import com.example.snack_mall.service.INewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@RestController
@RequestMapping("/new")
@Api(tags = "新品上线")
public class NewController {
    @Resource
    private INewService newService;

    @ApiOperation(value = "新品上线添加")
    @PostMapping("/reg")
    public Result reg(@RequestBody New fnew) {
        return newService.reg(fnew);
    }

    @ApiOperation(value = "新品上线编辑")
    @PutMapping("/edit")
    public Result edit(@RequestBody New fnew) {
        return newService.edit(fnew);
    }

    @ApiOperation(value = "新品上线删除")
    @DeleteMapping("/del/{id}")
    public Result delete(@PathVariable int id) {
        return newService.delete(id);
    }
    @ApiOperation(value = "新品上线批量删除")
    @PostMapping("/deleteMany")
    public Result deleteMany(@RequestBody List<Integer> ids){
        return newService.deleteMany(ids);
    }
    @ApiOperation(value = "新品上线分页查询")
    @GetMapping("/findPage")
    public Result findPage(Page<New> page){
        return newService.findPage(page);
    }

    @ApiOperation(value = "新品推荐的商品信息")
    @GetMapping("/newGoods")
    public Result newGoods(){
        return newService.newGoods();
    }
}

