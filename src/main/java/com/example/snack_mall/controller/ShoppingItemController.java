package com.example.snack_mall.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.ShoppingItem;
import com.example.snack_mall.entity.User;
import com.example.snack_mall.service.IShoppingItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@RestController
@RequestMapping("/shopping-item")
@Api(tags = "购物车")
public class ShoppingItemController {

    @Resource
    IShoppingItemService shoppingItemService;

    @ApiOperation(value = "购物车页面渲染")
    @GetMapping("/cart/{id}")
    public Result selectCart(@PathVariable int id){
        return Result.ok(shoppingItemService.selectCart(id));
    }

    @ApiOperation(value = "添加商品到购物车")
    @PostMapping("/addCart")
    public Result addCart(@RequestBody ShoppingItem shoppingItem){
        return shoppingItemService.addCart(shoppingItem);
    }

    @ApiOperation(value = "商品加")
    @PutMapping("/add")
    public Result add(Integer cartItemId,Integer goodsCount){
        return shoppingItemService.add(cartItemId,goodsCount);
    }

    @ApiOperation(value = "商品减")
    @PutMapping("/minus")
    public Result minus(Integer cartItemId,Integer goodsCount){
        return shoppingItemService.minus(cartItemId,goodsCount);
    }

    @ApiOperation(value = "单个删除购物车")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable int id){
        return shoppingItemService.delete(id);
    }

    @ApiOperation(value = "删除多个购物车")
    @DeleteMapping("/deleteMany")
    public Result delete(@RequestBody List<Integer> ids){
        return shoppingItemService.deleteMany(ids);
    }

    @ApiOperation(value = "删除多个购物车")
    @DeleteMapping("/clear")
    public Result clear(Integer userId){
        return shoppingItemService.clear(userId);
    }
}

