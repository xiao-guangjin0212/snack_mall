package com.example.snack_mall.controller;


import com.example.snack_mall.service.ICarouselService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.snack_mall.common.Result;
import com.example.snack_mall.entity.Carousel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author XGJ
 * @since 2024-01-04
 */
@RestController
@RequestMapping("/carousel")
@Api(tags = "轮播图")
public class CarouselController {
    @Resource
    private ICarouselService carouselService;

    @ApiOperation(value = "轮播图新增")
    @PostMapping("/reg")
    public Result reg(@RequestBody Carousel carousel){
        return carouselService.reg(carousel);
    }

    @ApiOperation(value = "轮播图删除")
    @DeleteMapping("/delete/{id}")
    public Result delete(@PathVariable("id") int id){
        return carouselService.del(id);
    }

    @ApiOperation(value = "轮播图批量删除")
    @PostMapping("/deleteMany")
    public Result deleteMany(@RequestBody List<Integer> ids){
        return carouselService.deleteMany(ids);
    }

    @ApiOperation(value = "轮播图编辑")
    @PutMapping("/edit")
    public Result edit(@RequestBody Carousel carousel){
        return carouselService.edit(carousel);
    }

    @ApiOperation(value = "轮播图分页查询")
    @GetMapping("/findPage")
    public Result findPage(Page<Carousel> page){
        return carouselService.findPage(page);
    }

    @ApiOperation(value = "轮播图按rank排序")
    @GetMapping("/carouselUrl")
    public Result carouselUrl(){
        return carouselService.carouselUrl();
    }
}

