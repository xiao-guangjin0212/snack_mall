package com.example.snack_mall.mapper;

import com.example.snack_mall.entity.GoodsInfo;
import com.example.snack_mall.entity.ShoppingItem;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.List;


@SpringBootTest
class ShoppingItemMapperTest {

    @Resource
    ShoppingItemMapper shoppingItemMapper;

    @Test
    public void selectCart(){

        List<ShoppingItem> shoppingItem = shoppingItemMapper.selectCart(9);
        System.out.println(shoppingItem);
    }

}