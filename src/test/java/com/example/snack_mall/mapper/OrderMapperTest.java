package com.example.snack_mall.mapper;

import com.example.snack_mall.entity.GoodsInfo;
import com.example.snack_mall.entity.OrderItem;
import com.example.snack_mall.entity.OrderMsg;
import com.example.snack_mall.vo.OrderMsgVo;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class OrderMapperTest {

    @Resource
    OrderMapper orderMapper;
@Resource
OrderItemMapper orderItemMapper;
    @Test
    void selectOrderByUserId() {
        List<OrderMsgVo> orderMsgs = orderItemMapper.selectOrderByUserId(9);
        System.out.println(orderMsgs.size());
    }
}